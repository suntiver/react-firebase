import React from 'react'
import Login from './components/pages/Login/Login'
import auth from './firebase'

export default function App() {
  
  const [session, setSession] = React.useState({
    isLoggedIn:false,
    currentUser:null,
    errorMessage:null
  });

  React.useEffect(() => {
    
    const handleAuth = auth.onAuthStateChanged(user =>{

      if(user){
          setSession({
              isLoggedIn:true,
              currentUser:user,
              errorMessage:null
          })
      }

    });

    return () => {
          handleAuth();
    }
  }, [])

 
  return (
    <div>
        {
        session.isLoggedIn ? (<h1>Login สำเร็จ
            <h4>Hi ! ,{session.currentUser && session.currentUser.email}</h4>
             <button type="button" onClick={()=>{
               auth.signOut().then(respone =>{
                setSession({
                    isLoggedIn:false,
                    currentUser:null

                })

               });
             }}>ออกจากระบบ</button>

        </h1>) :(<Login  setSession={setSession} />)}
             {
               console.log(session)
             }
    </div>
  )
}
