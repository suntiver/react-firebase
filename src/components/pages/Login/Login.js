import React from 'react'
import auth from '../../../firebase/index'


export default function Login({setSession}) {

    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');

    const  hadleLogin  = async () => {
  
        try{
        const response = await auth.signInWithEmailAndPassword(
            username,
            password
          );
       const {user} = response ;

         setSession({
           isLoggedIn:true,
           currentUser:user
        
        })
        }catch(error){
            setSession({
                isLoggedIn:false,
                currentUser:null,
                errorMessage:error.errorMessage
            })
        }
        
        
       
    }

    const  hadleRegister = async () => {
  
        try{
        const response = await auth.createUserWithEmailAndPassword(
            username,
            password
          );
       const {user} = response ;

         setSession({
           isLoggedIn:true,
           currentUser:user
        
        })
        }catch(error){
            setSession({
                isLoggedIn:false,
                currentUser:null,
                errorMessage:error.errorMessage
            })
        }
        
        
       
    }

    const handleUsername = event =>{
        setUsername(event.target.value);
    }
    const handPassword = event =>{
        setPassword(event.target.value);
    }
    return (
        <div>
            <input type="email" placeholder="Email"  onChange={handleUsername} />
            <input type="passwod" placeholder="Password" onChange={handPassword} />

            <button type="button" onClick={hadleLogin} >Login</button>
            <button type="button" onClick={hadleRegister} >Register</button>
            
        </div>
    )
}
